require 'sinatra'
require 'sinatra/json'
require 'json'
require 'dm-core'
require 'dm-migrations'
require 'nokogiri'
require 'rubygems'
require 'drb/drb'
require_relative '/root/letmecallu/tts/ivona.rb'
DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/development.db")


class Client
  include DataMapper::Resource
  property :id, Serial
  property :userid, String, :unique => true
  property :password, String, :required => true
  property :name, String
  property :description, Text
end
DataMapper.finalize



set :bind, "0.0.0.0"
set :port, "80"

post '/fsapi' do
@domain = params[:domain]
@userid = params[:sip_auth_username]
@action = params[:action]
content_type 'text/xml'

puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  #{params}  <<<<<<<<<"

case @action.downcase

when "sip_auth"
puts "<document type=\"freeswitch/xml\">
  <section name=\"directory\">
    <domain name=\"#{@domain}\">
       <groups>
        <group name=\"default\">
         <users>
         <user id=\"#{@userid}\">
                <variables>
                      <variable name=\"user_context\" value=\"default\"/>
            </variables>
         </user>
         </users>
        </group>
      </groups>
    </domain>
  </section>
</document>"

puts "------------------------------------------------"


return "<document type=\"freeswitch/xml\">
  <section name=\"directory\">
    <domain name=\"#{@domain}\">
       <groups>
        <group name=\"default\">
         <users>
	 <user id=\"#{@userid}\">
		<params>
      			<param name=\"dial-string\" value=\"{^^:sip_invite_domain=${dialed_domain}:presence_id=${dialed_user}@${dialed_domain}}${sofia_contact(*/${dialed_user}@${dialed_domain})}\"/>
		</params>

                <variables>
                      <variable name=\"user_context\" value=\"default\"/>
            </variables>
	 </user>
         </users>
        </group>
      </groups>
    </domain>
  </section>
</document>"

when "user_call" 
puts "<document type=\"freeswitch/xml\">
  <section name=\"directory\">
    <domain name=\"#{@domain}\">
       <groups>
        <group name=\"default\">
         <users>
         <user id=\"#{params[:user]}\">
                <variables>
                      <variable name=\"user_context\" value=\"default\"/>
            </variables>
         </user>
         </users>
        </group>
      </groups>
    </domain>
  </section>
</document>"

puts "-----------------------------------------------"


return "<document type=\"freeswitch/xml\">
  <section name=\"directory\">
    <domain name=\"#{@domain}\">
       <groups>
        <group name=\"default\">
         <users>
         <user id=\"#{params[:user]}\">
                <variables>
                      <variable name=\"user_context\" value=\"default\"/>
            </variables>
         </user>
         </users>
        </group>
      </groups>
    </domain>
  </section>
</document>"


when "reverse-auth-lookup"

puts "<document type=\"freeswitch/xml\">
  <section name=\"directory\">
    <domain name=\"#{@domain}\">
      <user id=\"#{params[:user]}\">
        <params>
           <param name=\"reverse-auth-user\" value=\"#{params[:user]}\" />
#           <param name=\"reverse-auth-pass\" value=\"dummy_password\" />
        </params>
      </user>
    </domain>
  </section>
</document>"


return "<document type=\"freeswitch/xml\">
  <section name=\"directory\">
    <domain name=\"#{@domain}\">
      <user id=\"#{params[:user]}\">
        <params>
           <param name=\"reverse-auth-user\" value=\"#{params[:user]}\" />
#           <param name=\"reverse-auth-pass\" value=\"dummy_password\" />
        </params>
      </user>
    </domain>
  </section>
</document>"

end


end

post '/delete_user' do
$status = "success"
$reason = "not applicable"
@uid = params[:username]
@company_id = params[:company_id]
@fname = "/usr/local/freeswitch/conf/directory/default/"+@company_id + "/" + @uid + ".xml"
begin
File.delete(@fname)
rescue Exception => e
	$status = "failed"
	$reason = "user does not exist" 
end
content_type :json
{ :status => $status , :reason => $reason}.to_json


end

post '/edit_user' do
$status = "success"
$reason = "not applicable"
@uid = params[:username]
@company_id = params[:company_id]
@fname = "/usr/local/freeswitch/conf/directory/default/"+@company_id + "/" + @uid + ".xml"
begin
profile = File.read(@fname)
puts profile
profile = profile.gsub( /password.*\>/  ,  "password\" value=\"#{params[:password]}\"/>")
puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#{File.write(@fname, profile)}"
system("/usr/local/freeswitch/bin/fs_cli -x reloadxml")

rescue Exception => e
        $status = "failed"
        $reason = "user does not exist or error #{e}"
end
content_type :json
{ :status => $status , :reason => $reason}.to_json


end



post '/create_user' do

@uid = params[:randUser] || params[:username]
@pwd = params[:randPass] || params[:password]
@company_id = params[:company_id]
@fname = "/usr/local/freeswitch/conf/directory/default/"+ @company_id+"/" + @uid + ".xml"
$status = "success"
$reason = "not applicable"
# username/company_id  should not contain @ , and no space either
if !@uid.match(/\@|\$|\%|\^|\*|\!|\~|\+|\=|\-|\(|\)|\`|[ ]+|^$/).nil? || !@company_id.match(/\@|\$|\%|\^|\*|\!|\~|\+|\=|\-|\(|\)|\`|[ ]+|^$/).nil? || !@pwd.match(/[ ]+|^$/).nil?
	$status = "failed"
	$reason = "space and special character not allowed in username/company_id"
elsif  @uid.nil? || @pwd.nil? || @company_id.nil?
	$status = "failed"
	$reason = "one of the field username||password||company_id is not supplied"
elsif File.exist? @fname 
	$status = "failed"
	$reason = "user already exist"
	
else
#if a new company create a new folder in directory/default
if !File.exist? "/usr/local/freeswitch/conf/directory/default/"+ @company_id
	system("mkdir " + "/usr/local/freeswitch/conf/directory/default/"+ @company_id)
end
outFile = File.new(@fname, "w")

File.chown(501, 159, @fname) 
outFile.puts("<include>
  <user id=\"" + @uid + "\">
    <params>
      <param name=\"password\" value=\"" + @pwd + "\"/>
      <param name=\"vm-password\" value=\"" + @uid + "\"/>
    </params>
    <variables>
      <variable name=\"toll_allow\" value=\"domestic,international,local\"/>
      <variable name=\"accountcode\" value=\"" + @uid + "\"/>
      <variable name=\"user_context\" value=\"default\"/>
      <variable name=\"effective_caller_id_name\" value=\"Extension " + @uid + "\"/>
      <variable name=\"effective_caller_id_number\" value=\"" + @uid + "\"/>
      <variable name=\"outbound_caller_id_name\" value=\"$${outbound_caller_name}\"/>
      <variable name=\"outbound_caller_id_number\" value=\"$${outbound_caller_id}\"/>
      <variable name=\"callgroup\" value=\""+@company_id +"\"/>
    </variables>
  </user>
</include>")
outFile.close
system("/usr/local/freeswitch/bin/fs_cli -x reloadxml")
end
content_type :json
  { :status => $status , :reason => $reason}.to_json
end


post '/create_conference' do

@cid = params[:confNumber]
@annId = params[:annFile]
@fname = "/usr/local/freeswitch/conf/dialplan/public/" + @cid + ".xml"
outFile = File.new(@fname, "w")
File.chown(501, 159, @fname) 
outFile.puts("
  <include>
    <extension name=\"conferences_" + @cid + "\">
      <condition field=\"destination_number\" expression=\"^(" + @cid + ")$\">
        <action application=\"answer\"/>
	<action application=\"playback\" data=\"/root/freeswitch/uploads/" + @annId + "\"/>        
	<action application=\"conference\" data=\"$1-${domain_name}@default\"/>
      </condition>
    </extension>
  </include>
  ")
outFile.close
system("/usr/local/freeswitch/bin/fs_cli -x reloadxml")
erb :close

end


get '/upload' do
    erb :upload
end

post '/upload' do
   if params[:file]
  filename = params[:file][:filename]
  tempfile = params[:file][:tempfile]
  target = "/root/freeswitch/uploads/#{filename}"
  puts target
  File.open(target, 'wb') {|f| f.write tempfile.read }
end
    'Yeaaup'
end

post '/conf/:id' do
  @confid = params[:id]
  return "<Response>
	  <Dial>
	    <Sip username=\"1001\" password=\"1234\">sip:" + @confid + "@54.213.58.94</Sip>
	  </Dial>
	</Response>"
end


post '/conference/invite' do
  
  account_sid = 'AC24524458296769320fedfe3e857c8e46'
  auth_token = '971c6f69d21425dcffdaa7ae39cff792'
  @to = params[:dialTo]
  @confid = params[:confName]
  @client = Twilio::REST::Client.new account_sid, auth_token
  @url = "http://54.213.58.94/conf/" + @confid

  call = @client.account.calls.create(:url => @url,
    :to => @to,
    :from => +14159682013)
  puts call.to

end

get '/live_calls' do
dump = `/usr/local/freeswitch/bin/fs_cli -x "show calls"`
response = []
count = 0
dump.split("\n").each do |entry|
        if count >0
                if entry.match(/\s/).nil?
                        break
                else
                        entry_arr = entry.split(",")
                        response =     response.push({count.to_s=> {"uuid" => entry_arr[0] , "start_stamp"=>entry_arr[2], "caller_id_number"=>entry_arr[7], "destination_number"=>entry_arr[9] , "callstate"=>entry_arr[12]   }} )
                end
        end
        count += 1
end
content_type :json
{"cdrs"=>response}.to_json
end


post '/incomming' do
  
  @did = params[:called]
  return "<Response>
	  <Dial>
	    <Sip username=\"1001\" password=\"1234\">sip:" + @did + "@54.213.58.94</Sip>
	  </Dial>
	</Response>"
  
end

get '/conference' do

@SERVER_URI="druby://127.0.0.1:8788"
@aleg = params[:call_id]
@cusername = params[:cparty]
@origin = params[:origin]
ahn = DRbObject.new_with_uri(@SERVER_URI)
puts "class of ahn object is #{ahn.inspect}"
ahn.conference(@origin,@aleg, @cusername)
end

get '/transfer' do

@SERVER_URI="druby://127.0.0.1:8788"
@aleg = params[:call_id]
@cusername = params[:cparty]
@origin = params[:origin]
ahn = DRbObject.new_with_uri(@SERVER_URI)
return ahn.transfer(@origin,@aleg, @cusername)
end

#create_welcome_prompt(company_id, type , data)
post '/create_prompts' do
puts "incoming request for create_welcome_prompt   #{params}"
status='success'
reason='do you need a reason for success'
=begin
if params[:company_id].nil? || params[:company_id]==''
	status='failed'
	reason='missing company_id'
elsif params[:type].nil? || params[:type]==''
	status='failed'
	reason='missing type'
elsif params[:data].nil? || params[:data]==''
	status='failed'
	reason='missing data'
elsif !(params[:type].strip.downcase!='tts' && params[:type].strip.downcase!='upload')
	status='failed'
	reason='type should be either tts or upload'
end
=end
if !(params[:general_type].nil? || params[:general_type]==''||params[:general_data].nil?||params[:general_data]=='')
	del = `rm -rf /var/punchblock/prompts/#{params[:company_id]}_general.*`

	if params[:general_type].downcase=='tts'
		ivona = Ivona.new('/var/punchblock/prompts')
		ivona.tts(params[:general_data], params[:company_id]+"_general.mp3")	
	elsif params[:general_type].downcase=='upload'
		`wget -N -O /var/punchblock/prompts/#{params[:company_id]}_general.#{params[:general_data].match(/[^\.]*$/)[0]} #{params[:general_data]}`
	else
		status='failed'
		reason='general_type should be tts||upload'
	end
end

if !(params[:afterhours_type].nil? || params[:afterhours_type]==''||params[:afterhours_data].nil?||params[:afterhours_data]=='')
        del = `rm -rf /var/punchblock/prompts/#{params[:company_id]}_afterhours.*`

        if params[:afterhours_type].downcase=='tts'
                ivona = Ivona.new('/var/punchblock/prompts')
                ivona.tts(params[:afterhours_data], params[:company_id]+"_afterhours.mp3")
        elsif params[:afterhours_type].downcase=='upload'
                `wget -N -O /var/punchblock/prompts/#{params[:company_id]}_afterhours.#{params[:afterhours_data].match(/[^\.]*$/)[0]} #{params[:afterhours_data]}`
	else
		status='failed'
		reason='afterhours_type should be tts||upload'
        end
end

if !(params[:weekend_type].nil? || params[:weekend_type]==''||params[:weekend_data].nil?||params[:weekend_data]=='')
        del = `rm -rf /var/punchblock/prompts/#{params[:company_id]}_weekend.*`

        if params[:weekend_type].downcase=='tts'
                ivona = Ivona.new('/var/punchblock/prompts')
                ivona.tts(params[:weekend_data], params[:company_id]+"_weekend.mp3")
        elsif params[:weekend_type].downcase=='upload'
                `wget -N -O /var/punchblock/prompts/#{params[:company_id]}_weekend.#{params[:weekend_data].match(/[^\.]*$/)[0]} #{params[:weekend_data]}`
	else
		status='failed'
		reason='weekend_type should be tts||upload'
        end
end

content_type :json
{:status=>status, :reason=> reason}.to_json
end

post '/request_call' do

  @SERVER_URI="druby://127.0.0.1:8788"
  @aparty = params[:aParty]
  @bparty = params[:bParty]
  @type = params[:type]
  DRb.start_service
  ahn = DRbObject.new_with_uri(@SERVER_URI)
  ahn.outbound(@aparty,@bparty,@type)
  r  = Redis.new
  r.subscribe 'inbox' do |on|
   on.message do |channel , msg|
        puts msg
         return  msg
   end
end
  redirect '/'

end

get '/login_status/:agent' do
content_type :json
`ls`
puts "fs_cli -x \"sofia status profile internal reg  #{params["agent"]}\""
result = %x[/usr/local/freeswitch/bin/fs_cli -x "sofia status profile internal reg  #{params["agent"]}"]
if result.match(/Total items returned:.*\n/)[0].match(/\d/)[0].to_i > 0
return {:login_status=> 'yes'}.to_json
else
return {:login_status=> 'no'}.to_json
end

end
