require 'json'
require_relative './ESL'
require_relative '../lib/hillstock.rb'
class AhnManager
	@@freeswitch
	def initialize
		@@freeswitch = ESL::ESLconnection.new('127.0.0.1', '8021', 'ClueCon')
	end	
	
	def live_calls
		@@freeswitch.sendRecv('api show calls').getBody
	end

	def conference(origin,call_id, cparty )
		c_dial_string = `fs_cli -x "sofia_contact #{cparty}"`.strip
		other_call_id = @@freeswitch.sendRecv("api uuid_getvar #{call_id} bridge_uuid ").getBody.strip
		logger.info "A -> #{call_id}  B-> #{other_call_id} C -> #{cparty} "
		Adhearsion.active_calls[call_id]=nil
		Adhearsion.active_calls[other_call_id]=nil
		 @@freeswitch.sendRecv("api uuid_transfer #{call_id} -both 0000#{cparty} XML public")
		logger.info "------------------------->"+ `fs_cli -x  "conference 0000#{cparty} dial #{c_dial_string}  #{origin}"`
	end	

        def transfer(origin,call_id, cparty )
                c_dial_string = `fs_cli -x "sofia_contact #{cparty}"`.strip
                other_call_id = @@freeswitch.sendRecv("api uuid_getvar #{call_id} bridge_uuid ").getBody.strip
                logger.info "A -> #{call_id}  B-> #{other_call_id} C -> #{cparty} "
                Adhearsion.active_calls[call_id]=nil
                Adhearsion.active_calls[other_call_id]=nil
                 @@freeswitch.sendRecv("api uuid_transfer #{call_id} -both 0000#{cparty} XML public")
                dial_result= `fs_cli -x  "conference 0000#{cparty} dial #{c_dial_string}  #{origin}"`
	logger.info "^^^^^^^^^^^^^^^^^^^ #{dial_result}"
		sleep(3)
		@@freeswitch.sendRecv("api uuid_kill #{call_id}")
	end
	def showActiveCall()
	logger.ifo"+++++++++++++++++++++++  #{Adhearsion.active_calls}"
	end

	def demo_callback(demo_did,audience )
		logger.info "call back request for Hillstock from #{audience}"
		Adhearsion::OutboundCall.originate "sofia/external/0011103#{audience}@sbc.voxbeam.com" , controller:Hillstock
	end
	
	        def dial_string(user)
                `fs_cli -x "sofia_contact #{user}"`.strip
        end


	        def outbound(aParty, bParty, type)

                begin
                logger.info "<<<<<<<>>>>>>>>> Connecting #{aParty} and #{dial_string(bParty)}"

                if type == 'internal'
                        dialOut = dial_string(bParty)
                        dialin = dial_string(aParty)
                        if dialOut[0..18] != 'sofia/internal/sip:'
                                publish('User not found', aParty)
                        elsif
                                logger.info "<<<<<<<DIAL LOCAL>>>>>>>>> Connecting #{dialOut}"
                                cstatus = Adhearsion::OutboundCall.originate dialOut, from: aParty, timeout: 10 do
                                        answer
                                        dial dial_string(aParty), from: bParty
                                end
                                cstatus.on_end do
                                        logger.info "<<<<<<<>>>>>>>>> Status : #{cstatus.end_reason} & #{aParty}"
                                        publish(cstatus.end_reason, aParty)
                                end
                        end

                elsif type == 'external'
                        dialOut = 'sofia/internal/sip:' + bParty + '@54.213.180.95'
                        logger.info "<<<<<<<DIAL OUT>>>>>>>>> Connecting #{dialOut}"
                        cstatus = Adhearsion::OutboundCall.originate dialOut, from: aParty, timeout: 20 do
                                answer
                                dial dial_string(aParty), from: bParty
                        end
                        cstatus.on_end do
                                logger.info "<<<<<<<>>>>>>>>> Status : #{cstatus.end_reason} & #{aParty}"
                                publish(cstatus.end_reason, aParty)
                        end
                end

                rescue exception=>e
                logger.info "<<<<<<<**********************>>>>>>>>> exceptions : #{e}"
                end
        end

end
