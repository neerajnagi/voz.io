require 'json'
require 'digest/sha1'
require_relative '../tts/ivona.rb'
class IvrManager

	@@lib_dir = "/root/clicktocall/lib"
	@@menu_timeout = 60
	@@ask_timeout = 60
	@@menu_tries = 3
        @@recording_dir = "/tmp"
	@@ivona = Ivona.new(@@recording_dir)
def tts(text)

file_name = Digest::SHA1.hexdigest(text) + ".mp3"
if !File.exist? @@recording_dir+"/"+file_name
	@@ivona.tts(text, file_name)
end
return @@recording_dir+"/"+file_name
end
	def add_controller(_data)
                _ivm = JSON.parse(_data)
		logger.info _ivm 
                if !File.exist?(@@lib_dir+'/'+_ivm['name'].downcase + '.rb' )
			if system('cd #{@@lib_dir}')
                        	if system('ahn generate controller ' + _ivm['name'])
                                	puts "controller #{_ivm['name']} generated"
	                        end
			end     	
		end
                puts 'controller file exists'
                f = File::open(@@lib_dir+'/'+_ivm['name'].downcase + '.rb','w')

                if(f)
	                puts 'successfully opened controller file for editing'
                        f.write("# encoding: utf-8\nclass #{_ivm['name'].capitalize} < Adhearsion::CallController\ndef run\nanswer\n")
                        add_controller_helper(JSON.parse(_ivm['ivm']),f,1)
                        f.write("end\nend")
                        f.close()
                        # once a new controller is generated or existing controller modified we should re-include it
	                puts "updating controller repository"   
                        load @@lib_dir+'/'+_ivm['name'].downcase+".rb"
        	end
        end
        private
        
        def add_controller_helper(_data, f , level)
                        puts _data
                        if _data['total'] == 0
                                return
                        end             
                        
        
                        ivm = _data
                        #f.write("# #{ivm['id']}\n")
                        for i in 1..ivm['total']
                                
                                task = ivm[i.to_s()]
                                marker='start'
                                
                                
                                case ivm[i.to_s()]['type']
                                        when    'say'
                                                level.times do f.write("\t") end
                                                f.write("play \"#{tts(task['msg'])}\"\n")
                                        when	'dial'
						level.times do f.write("\t") end
						f.write("dial \"sofia/external/#{task['msg']}@54.213.58.94\"\n")       
                                        when    'ask'
                                                level.times do f.write("\t") end
                                                if task['method']=='digit'
                                                        f.write("#{task['var']} = ask \"#{tts(task['msg'])}\" , :limit=>#{task['method_var']} , :timeout=>#{@@ask_timeout}\n")
                                                else
                                                        f.write("#{task['var']} = ask \"#{tts(task['msg'])}\" , :terminator=>'#{task['method_var']}' , :timeout=>#{@@ask_timeout}\n")
                                                end
                                        when    'menu'
                                                level.times do f.write("\t") end
                                                f.write("menu \"#{tts(task['msg'])}\" , :timeout=>#{@@menu_timeout}.seconds , :tries=>#{@@menu_tries} do\n")
                                                for j in 1..task['total']
                                                        f.write("# #{task['list'][j.to_s()]['id']}  ,  #{task['list'][j.to_s()]['desc']}\n")
                                                        (level).times do f.write("\t") end
                                                        f.write("\tmatch #{task['list'][j.to_s()]['press']} do |x|\n")
                                                                
                                                                add_controller_helper(task['list'][j.to_s()]['start'] , f,level+2)
                                                        
                                                        level.times do f.write("\t") end        
                                                        f.write("\tend\n")
                                                
                                                end
                                                        level.times do f.write("\t") end
                                                        f.write("end\n")
                                        else
                                end
                        
                        end
        
        end     
                        
        
        
end
