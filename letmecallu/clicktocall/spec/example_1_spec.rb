# encoding: utf-8

require 'spec_helper'

describe Example-1 do

  let(:mock_call) { mock 'Call', :to => '1112223333', :from => "2223334444" }
  let(:metadata) { {} }
  subject { Example-1.new(mock_call, metadata) }

  it "should have empty metadata" do
    subject.metadata.should eq({})
  end

end
