# encoding: utf-8
require_relative '../media/aws.rb'
require 'mechanize'
require 'json'
require 'redis'
require 'securerandom'
GET_COMPANY_ID = "http://93.170.50.103:3000/api/export_companies?number="
GET_COMPANY_AGENTS = "http://93.170.50.103:3000/api/export_agents?company="
POST_CALL_RECORDING = "http://93.170.50.103:3000/api/update_calls"
LETMECALLU_RECORDING_BUCKET = "https://s3-us-west-2.amazonaws.com/letmecallu-recordings/"

ACL = "206.15.130.13 208.64.8.13"
class Incomming < Adhearsion::CallController
	def run
		logger.info "incoming call from #{call.from.match(/<.*>/)[0].gsub(/[<>]*|\@.*$/,'')}  -->  #{call.to.gsub(/\@.*$/,'')}"
		origin_ip = call.variables[:x_variable_sip_network_ip]||"_blank"
		to_number = call.to.gsub(/\@.*$/,'')
		from_number = call.from.match(/<.*>/)[0].gsub(/[<>]*|\@.*$/,'')
		sip_agent = call.variables[:x_variable_sip_user_agent]||'voxbeam'
		call_id = call.variables[:x_variable_uuid]		
		sip_call_id = call.variables[:x_variable_sip_call_id]	
		logger.info "sip agent is --------------- #{sip_agent} origin ip is -------- #{origin_ip}"
		aws = Aws.new
		browser = Mechanize.new
#first segregate call treatment based on sip agent , one category is calls from sipML5 and second is everything else
		if ACL.include? origin_ip
			logger.info "incoming call forwarded by phonepower  from #{from_number}"
			play "/var/punchblock/record/onepgr_welcome.mp3"
			play "/var/punchblock/record/onepgr_conference_code.mp3"
			answer
			conference_code = ask  "/var/punchblock/record/onepgr_beep.mp3" , :limit=>6  , :timeout=>30.seconds 
			logger.info "conference_code is #{conference_code.inspect}"
			dial "{absolute_codec_string=PCMU}sofia/internal/onepgr_#{conference_code.response[0,6]}@172.31.41.127"
			play "/var/punchblock/record/onepgr_notfound.mp3"
			hangup
		elsif sip_agent.include? "Twilio"
		pass Kernel.const_get(to_number) 
		#call treatment for incoming calls forwarded by twilio will be handled here
		#	answer
		#	play "/var/punchblock/record/twilio.mp3"


		elsif( !((sip_agent.include? "sipML5") || (sip_agent.include? "X-Lite") )  )
		if to_number=='14157021304'
			 logger.info "incoming call forwarded by phonepower  from #{from_number}"
                        play "/var/punchblock/record/onepgr_welcome.mp3"
                        conference_code = ask  "/var/punchblock/record/onepgr_beep.mp3" , :limit=>10 
                        logger.info "conference_code is #{conference_code.inspect}"
                        dial "sofia/external/#{conference_code.response[0-4]}"
                        play "/var/punchblock/record/onepgr_notfound.mp3"
                        hangup
			logger.info "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! incoming call for Hillstock"
			pass Kernel.const_get("Hillstock")
		else

		company_metadata = JSON.parse(browser.get(GET_COMPANY_ID+to_number.to_s).body)['companies']
			if !company_metadata.nil?
				  agents = JSON.parse(browser.get(GET_COMPANY_AGENTS+company_metadata[0]['id'].to_s).body)['agents']||[]
			end
			answer
			 play Dir['/var/punchblock/welcome_prompts/' + company_metadata[0]['id'].to_s + '.*' ][0]  
			logger.info "company metadata is -> #{ company_metadata}"
			logger.info "comapny  agents are -> #{ agents}"
			available_agents = []
			if !agents.nil?
				agents.each do |agent|
				        if  (`fs_cli -x "sofia status profile internal reg  #{agent['sip_user']}"`.match(/Total items returned:.*\n/)[0].match(/\d/)[0].to_i > 0 ) || (agent['status'] == 'on_the_phone')
			               		 available_agents.push(agent)
					    	    logger.info "agent available #{agent['sip_user']}"
				        end
				end
			end
			if available_agents.length ==0
				answer
				play "/var/punchblock/record/all_busy.mp3"
				hangup
			else
				case (company_metadata[0]['call_distribution'])
				        when 'roundrobin'
						redis = Redis.new
						available_agents = available_agents.sort_by { |hsh| hsh[:sip_user] }.reverse
						logger.info "available agents in alphabetical order are  -----> #{available_agents}"
		 				who_served_last_time = redis.get(to_number)
						who_serves_this_time = {}
						if who_served_last_time.nil?
							who_serves_this_time = available_agents[0]
							redis.set(to_number, who_serves_this_time)
						else
							available_agents.each do |a_agent|
								if a_agent['sip_user'] > who_served_last_time
									who_serves_this_time = a_agent
									redis.set(to_number, who_serves_this_time)
									break
								end
							end
							if who_serves_this_time=={} #start again from start of array
								 who_serves_this_time = available_agents[0]
        	                                                redis.set(to_number, who_serves_this_time)
	
							end
						end	
						logger.info "who serves this time ------------------> #{who_serves_this_time}"				
				                if( who_serves_this_time['status'] != 'on_the_phone')
							dest = `fs_cli -x "sofia_contact #{who_serves_this_time['sip_user']}"`.strip
						else
							dest = "sofia/external/0011103#{who_serves_this_time['phone'].gsub(/^\+|^00/,'')}@sbc.voxbeam.com" 
						end
				        when 'simultaneous'
						dest = []
				                available_agents.each do |agent|
			        	                if agent['status'] == 'on_the_phone'
								dest.push "sofia/external/0011103#{agent['phone'].gsub(/^\+|^00/,'')}@sbc.voxbeam.com"
							else	
								dest.push `fs_cli -x "sofia_contact #{agent['sip_user']}"`.strip 
							end	                
						end
					else 
						dest = []
                                                available_agents.each do |agent|
                                                        if agent['status'] == 'on_the_phone'
                                                                dest.push "sofia/external/0011103#{agent['phone'].gsub(/^\+|^00/,'')}@sbc.voxbeam.com"
                                                        else    
                                                                dest.push `fs_cli -x "sofia_contact #{agent['sip_user']}"`.strip                                     
                                                        end 
                                                end
				end
				#recording agent conversation async mode, default record directory is /var/punchblock/record
				record async: true do |event|
				      logger.info "Async recording for call id #{sip_call_id}  saved to #{event.recording.uri}"
					`sox #{event.recording.uri.gsub(/^.*var/,'/var')}   -c 1 -b8 -U -t .wav -r 8000 #{event.recording.uri.gsub(/^.*var/,'/var').gsub(/\.wav$/,'_mono.wav')}`
					aws.push_recording("#{sip_call_id}.wav" , "#{event.recording.uri.gsub(/^.*var/,'/var').gsub(/\.wav$/,'_mono.wav')}")
					browser.post(POST_CALL_RECORDING,{"did"=>to_number  , "call_id"=> sip_call_id ,"file_path"=>LETMECALLU_RECORDING_BUCKET+sip_call_id+".wav" })
				`rm -rf #{event.recording.uri.gsub(/^.*var/,'/var')}`
				`rm -rf #{event.recording.uri.gsub(/^.*var/,'/var').gsub(/\.wav$/,'_mono.wav')}`
				end
				dial dest , from: from_number
				hangup
			end

#dial `fs_cli -x "sofia_contact 1005"`.strip
end
		else
			logger.info "call from sipML5 agent"
			answer
			play! "/var/punchblock/record/moh.wav"
			 record async: true do |event|
                                      logger.info "Async recording for call id #{call_id}  saved to #{event.recording.uri}"
                                        aws.push_recording("#{call_id}.wav" , event.recording.uri.gsub(/^.*var/,'/var'))
                                        browser.post(POST_CALL_RECORDING,{"call_id"=> sip_call_id ,"file_path"=>LETMECALLU_RECORDING_BUCKET+sip_call_id+".wav" })
                                end

			to = call.to.gsub(/\:.*/,'').gsub(/\@.*/,'')
			if( !to.match(/^00.*$/).nil? )
				dial "sofia/external/0011103#{to.strip.gsub(/^00/,'')}@sbc.voxbeam.com" , from: "09866154296"
				hangup
			elsif ( !to.match(/^\+.*$/).nil? )
			        dial "sofia/external/0011103#{to.strip.gsub(/^\+/,'')}@sbc.voxbeam.com" , from: "09866154296"
		        	hangup
			else  
				sip_uri = `fs_cli -x \"sofia_contact #{call.to.gsub(/\:.*/,'')}\"`.strip
				dial_status = 	dial "{hangup_after_bridge=false}#{sip_uri}" , from: from_number 
				logger.info "dial_status is #{dial_status.inspect }"
				hangup
			end
		end
	end
end
