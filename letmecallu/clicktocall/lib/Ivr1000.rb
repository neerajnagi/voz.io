# encoding: utf-8

class Ivr1000 < Adhearsion::CallController
def run
answer
	menu "/tmp/6d240cb87b32797f0c8b494f3aafd50b6181322b.mp3" , :timeout=>60.seconds , :tries=>3 do
# start_1  ,  1001
		match 1 do |x|
			menu "/tmp/3340c60b1fa833792337d55bcf3bb37182c5bce6.mp3" , :timeout=>60.seconds , :tries=>3 do
# start_1_3  ,  1003
				match 3 do |x|
					play "/tmp/c22b5f9178342609428d6f51b2c5af4c0bde6a42.mp3"
					conference_id = ask "/tmp/249dcf4842949e51ec623cff247a663ffe31ee5b.mp3" , :limit=>4 , :timeout=>60
					dial "sofia/internal/2345"
					dial "sofia/internal/3467"
				end
# start_1_4  ,  1004
				match 4 do |x|
				end
			end
		end
# start_2  ,  1002
		match 2 do |x|
			dial "sofia/internal/1235"
			dial "sofia/internal/1005"
		end
	end
end
end