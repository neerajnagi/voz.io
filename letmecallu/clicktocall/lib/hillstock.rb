# encoding: utf-8
class Hillstock < Adhearsion::CallController
def run
answer
	menu "/tmp/ab5d58245656f6e7079e5b4b4641f0ca9e993488.mp3" , :timeout=>60.seconds , :tries=>3 do
# start_1  ,  Principal&#x27;s Secratary
		match 1 do |x|
			play "/tmp/2ceffbc85ed22956fd8a95f453218d20bd779c80.mp3"
		end
# start_2  ,  Alumni Representative
		match 2 do |x|
			dial "sofia/external/+14157021300@54.213.58.94"
		end
# start_3  ,  Support Staff
		match 3 do |x|
			menu "/tmp/e255f9aaa75d34d059d863a8cb9ef019b4905f83.mp3" , :timeout=>60.seconds , :tries=>3 do
# start_3_1  ,  Electricity 
				match 1 do |x|
					dial "sofia/external/+14157021301@54.213.58.94"
				end
# start_3_2  ,  Maintainence
				match 2 do |x|
					dial "sofia/external/+14157021302@54.213.58.94"
				end
# start_3_3  ,  Telephone
				match 3 do |x|
					play "/tmp/35be84593d6cd5d0c569416947804861d5e4b44e.mp3"
				end
# start_3_4  ,  Technology
				match 4 do |x|
					menu "/tmp/050a2141f231a05d3f3c806461ed357f89758f3c.mp3" , :timeout=>60.seconds , :tries=>3 do
# start_3_4_1  ,  Laptop
						match 1 do |x|
							dial "sofia/external/+14157021303@54.213.58.94"
						end
# start_3_4_2  ,  Internet
						match 2 do |x|
							play "/tmp/2510a1455d8396d6df66a43ccbdbff76765ea416.mp3"
						end
					end
				end
			end
		end
	end
end
end
