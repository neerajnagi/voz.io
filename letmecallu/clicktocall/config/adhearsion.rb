# encoding: utf-8

Adhearsion.config do |config|

  # Centralized way to specify any Adhearsion platform or plugin configuration
  # - Execute rake config:show to view the active configuration values
  #
  # To update a plugin configuration you can write either:
  #
  #    * Option 1
  #        Adhearsion.config.<plugin-name> do |config|
  #          config.<key> = <value>
  #        end
  #
  #    * Option 2
  #        Adhearsion.config do |config|
  #          config.<plugin-name>.<key> = <value>
  #        end

  config.development do |dev|
    dev.platform.logging.level = :debug
  end

  ##
  # Use with Rayo (eg Voxeo PRISM)
  #
  # config.punchblock.username = "" # Your XMPP JID for use with Rayo
  # config.punchblock.password = "" # Your XMPP password

  ##
  # Use with Asterisk
  #
  # config.punchblock.platform = :asterisk # Use Asterisk
  # config.punchblock.username = "" # Your AMI username
  # config.punchblock.password = "" # Your AMI password
  # config.punchblock.host = "127.0.0.1" # Your AMI host

  ##
  # Use with FreeSWITCH
  #
   config.punchblock.platform = :freeswitch # Use FreeSWITCH
   config.punchblock.password = "ClueCon" # Your Inbound EventSocket password
   config.punchblock.host = "127.0.0.1" # Your IES host
end

Adhearsion::Events.draw do

  # Register global handlers for events
  #
  # eg. Handling Punchblock events
  # punchblock do |event|
  #   ...
  # end
  #
  # eg Handling PeerStatus AMI events
  # ami :name => 'PeerStatus' do |event|
  #   ...
  # end
  #
# anything started in after_initialized will be loaded once ahn is done loading whole environment, so we can safely run anything now
   after_initialized do |event|
	require 'drb/drb'
	require_relative '../drb/ivr_manager.rb'
	require_relative '../drb/ahn_manager.rb'
	# The object that handles requests on the server
	IVR_MANAGER = IvrManager.new()
	# drb client conncetion at 8787 port can run any function on FRONT_OBJECT and they'll be executed in this environment
	DRb.start_service("druby://127.0.0.1:8787", IVR_MANAGER)
	AHN_MANAGER = AhnManager.new()
	DRb.start_service("druby://127.0.0.1:8788", AHN_MANAGER)
	# Wait for the drb server thread to finish before exiting.
	DRb.thread.join
		
   end



end

Adhearsion.router do

  #
  # Specify your call routes, directing calls with particular attributes to a controller
  #

  route 'default', Incomming
end
