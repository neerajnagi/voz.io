# encoding: utf-8

require 'bundler'
Bundler.setup
Bundler.require
API_URL="http://93.170.50.103:3001"
CERTIFICATE="/etc/pki/tls/certs/letmecallu.pem"
FS_CLI="/usr/local/freeswitch/bin/fs_cli"
OUTGOING_PROVIDER="208.43.27.71"
OUTGOING_PROVIDER_PREFIX=""
INCOMING_PROVIDERS="RJT-ASTG-04 , Vox Callcontrol "
SOX="/usr/bin/sox"
