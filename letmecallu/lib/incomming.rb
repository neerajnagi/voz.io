# encoding: utf-8
require_relative '../media/aws.rb'
require 'mechanize'
require 'json'
require 'redis'
require 'openssl'
GET_COMPANY_ID = "#{API_URL}/api/export_companies?number="
GET_COMPANY_AGENTS = "#{API_URL}/api/export_agents?company="
GET_ALL_AGENTS = "#{API_URL}/api/export_agents"
POST_CALL_RECORDING = "#{API_URL}/api/update_calls"
POST_VOICEMAIL_RECORDING = "#{API_URL}/api/create_voicemail"
LETMECALLU_RECORDING_BUCKET = "https://s3-us-west-2.amazonaws.com/letmecallu-recordings/"
$redis = Redis.new
class ConfirmationController < Adhearsion::CallController
  def run
	require_relative '../media/aws.rb'
	aws = Aws.new
        browser = Mechanize.new
        browser.agent.verify_mode = OpenSSL::SSL::VERIFY_NONE

	redis = Redis.new
	logger.info "metadata passed on -> #{metadata}"
	logger.info "========= notification ========\n dial_result , answered \n ==============================="
	redis.publish "notification" , JSON.pretty_generate({from:metadata[:from] , to:metadata[:to] , event:{name:"dial_result", data:"answer"}  })

	 record async: true do |event|
                                      logger.info "Async recording for call id #{metadata[:sip_call_id]}  saved to #{event.recording.uri}"
					                                        `#{SOX} #{event.recording.uri.gsub(/^.*var/,'/var')}   -c 1 -w  -t .wav -r 16000 #{event.recording.uri.gsub(/^.*var/,'/var').gsub(/\.wav$/,'_mono.wav')}`
                                        aws.push_recording("#{metadata[:sip_call_id]}.wav" , "#{event.recording.uri.gsub(/^.*var/,'/var').gsub(/\.wav$/,'_mono.wav')}")

                                        logger.info "posted data to update_call" + browser.post(POST_CALL_RECORDING,{"call_id"=> metadata[:sip_call_id] ,"file_path"=>LETMECALLU_RECORDING_BUCKET+metadata[:sip_call_id]+".wav" }).content
                                end

  end
end

class Incomming < Adhearsion::CallController
	$from_number = ''
	$to_number = ''
	$company_id = ''
	$from_username = ""
	$to_username = ''
        def run
		answer
                logger.info "incoming call from #{call.from.match(/<.*>/)[0].gsub(/\<|\>|\@.*$/,'')}  -->  #{call.to.gsub(/\@.*$/,'')}"
                origin_ip = call.variables["x_variable_sip_received_ip"]||'_blank'
                $to_number = call.to.gsub(/\@.*$/,'')
                $from_number = call.from.match(/<.*>/)[0].gsub(/\<|\>|\@.*$/,'')
                $from_username = $from_number
		$to_username = $to_number
		sip_agent = call.variables[:x_variable_sip_user_agent]||'Vox'
                call_id = call.variables[:x_variable_uuid]
                sip_call_id = call.variables[:x_variable_sip_call_id]
                logger.info "sip agent is --------------- #{sip_agent}"
                aws = Aws.new
                browser = Mechanize.new
                browser.agent.verify_mode = OpenSSL::SSL::VERIFY_NONE
		logger.info "fetching company metadata at #{GET_COMPANY_ID+$to_number.to_s}"
                company_metadata = JSON.parse(browser.get(GET_COMPANY_ID+$to_number.to_s).body)
		logger.info "company metadata is #{company_metadata}"
                if company_metadata['status'] != 'fail'
			logger.info company_metadata.inspect
			$company_id = company_metadata['companies'][0]['id']
                        agents = JSON.parse(browser.get(GET_COMPANY_AGENTS+company_metadata['companies'][0]['id'].to_s).body)['agents']||[]
                end
		logger.info "************************************************ #{$company_id} ********"
#first segregate call treatment based on sip agent , one category is calls from sipML5 and second is everything else
                if(  INCOMING_PROVIDERS.include? sip_agent[0,6] )  #!((sip_agent.include? "sipML5") || (sip_agent.include? "JsSIP" )  || (sip_agent.include? "X-Lite") )  )
			if $company_id!=''
	                        if Dir["/var/punchblock/prompts/#{$company_id}*" ].length > 0
				   play  Dir["/var/punchblock/prompts/#{$company_id}*" ].grep /.*general.*/ 
                	        end
			end
                        logger.info "company metadata is -> #{ company_metadata}"
                        logger.info "comapny  agents are -> #{ agents}"
                        available_agents = []
                        if !agents.nil?
                                agents.each do |agent|
					logger.info "=========:::::=========\n#{agent}\n======================="
                                        if  (`#{FS_CLI} -x "sofia status profile internal reg  #{agent['sip_user']}"`.match(/Total items returned:.*\n/)[0].match(/\d/)[0].to_i > 0 ) || (agent['status'] == 'on_the_phone')
                                                 available_agents.push(agent)
                                                    logger.info "agent available #{agent['sip_user']}"
                                        end
                                end
                        end
                        if available_agents.length ==0
 #                         play "/var/punchblock/system/all_busy.mp3"
			             play "/var/punchblock/system/leave_message.mp3"
					play "/var/punchblock/system/beep.mp3"
					voicemail_start_time = Time.now.to_i
                                         event = record max_duration:120000
					voicemail_end_time = Time.now.to_i
                                         logger.info "Async recording for call id #{call_id}  saved to #{event.recording.uri}"
                                        aws.push_recording("#{call_id}.wav" , event.recording.uri.gsub(/^.*var/,'/var'))
                                        logger.info "voicemail saved to #{LETMECALLU_RECORDING_BUCKET+sip_call_id+".wav"}"
                                        logger.info "recording data posted to update_call" + browser.post(POST_VOICEMAIL_RECORDING,{call_id: sip_call_id ,company_id:$company_id, from:$from_number, to:$to_number,   recording_url:LETMECALLU_RECORDING_BUCKET+sip_call_id+".wav" , start_timestamp:voicemail_start_time, end_timestamp:voicemail_end_time }).content

                                hangup
                        else
                                case (company_metadata['companies'][0]['call_distribution'])
                                        when 'roundrobin'
						dest=''
						dest_username=''
                                                available_agents = available_agents.sort_by { |hsh| hsh[:sip_user] }.reverse
                                                logger.info "available agents in alphabetical order are  -----> #{available_agents}"
                                                who_served_last_time = $redis.get($to_number)
                                                who_serves_this_time = {}
                                                if who_served_last_time.nil?
                                                        who_serves_this_time = available_agents[0]
                                                        $redis.set($to_number, who_serves_this_time['sip_user'])
                                                else
							indx=0
							available_agents.each do | a_agent | 
								logger.info "+++++ #{who_served_last_time} #{a_agent['sip_user']}"
								if (a_agent['sip_user'].to_s == who_served_last_time.to_s)
									logger.info "match found"	
									if indx < (available_agents.length-1)
									indx = indx+1
									elsif indx==available_agents.length-1
									indx = 0 
									end
									break
								end
								indx = indx+1
							end
														
							if indx==available_agents.length
								indx=0
						
							end
							logger.info "agent with index #{indx} from available agents will server"
							who_serves_this_time = available_agents[indx]
                                                        $redis.set($to_number, who_serves_this_time['sip_user'])

                                                end
                                                logger.info "who serves this time ------------------> #{who_serves_this_time}"
                                                if( who_serves_this_time['status'] != 'on_the_phone')
                                                        dest = `#{FS_CLI} -x "sofia_contact #{who_serves_this_time['sip_user']}"`.strip
							dest_username = who_serves_this_time['sip_user']
                                                else
                                                        dest = "sofia/external/#{OUTGOING_PROVIDER_PREFIX}#{who_serves_this_time['phone'].gsub(/^\+|^00/,'')}@#{OUTGOING_PROVIDER}"
							dest_username = who_serves_this_time['phone']
                                                end
                                        when 'simultaneous'
                                                dest = []
						dest_username= []
                                                available_agents.each do |agent|
                                                        if agent['status'] == 'on_the_phone'
                                                                dest.push "sofia/external/#{OUTGOING_PROVIDER_PREFIX}#{agent['phone'].gsub(/^\+|^00/,'')}@#{OUTGOING_PROVIDER}"
								dest.push agent['phone']
                                                        else
                                                                dest.push `#{FS_CLI} -x "sofia_contact #{agent['sip_user']}"`.strip
								dest.push agent['sip_user']
                                                        end
                                                end
                                        else
                                                dest = []
                                                available_agents.each do |agent|
                                                        if agent['status'] == 'on_the_phone'
                                                                dest.push "sofia/external/#{OUTGOING_PROVIDER_PREFIX}#{agent['phone'].gsub(/^\+|^00/,'')}@#{OUTGOING_PROVIDER}"
                                                        else
                                                                dest.push `#{FS_CLI} -x "sofia_contact #{agent['sip_user']}"`.strip      
                                                        end
                                                end
                                end
                                #recording agent conversation async mode, default record directory is /var/punchblock/record
=begin
                                record async: true do |event|
                                      logger.info "Async recording for call id #{sip_call_id}  saved to #{event.recording.uri}"
                                     logger.info "Processing async all recording"
					   logger.info "#{SOX} #{event.recording.uri.gsub(/^.*var/,'/var')}   -c 1 -w  -t .wav -r 16000 #{event.recording.uri.gsub(/^.*var/,'/var').gsub(/\.wav$/,'_mono.wav')}"

					`#{SOX} #{event.recording.uri.gsub(/^.*var/,'/var')}   -c 1 -w  -t .wav -r 16000 #{event.recording.uri.gsub(/^.*var/,'/var').gsub(/\.wav$/,'_mono.wav')}`
					aws.push_recording("#{sip_call_id}.wav" , "#{event.recording.uri.gsub(/^.*var/,'/var').gsub(/\.wav$/,'_mono.wav')}")
                        		logger.info "::::::::::::::::: call recording is at ->" +LETMECALLU_RECORDING_BUCKET+sip_call_id+".wav"
			                logger.info "call recording data put to" + browser.post(POST_CALL_RECORDING,{"did"=>$to_number  , "call_id"=> sip_call_id ,"file_path"=>LETMECALLU_RECORDING_BUCKET+sip_call_id+".wav" }).content
                                `rm -rf #{event.recording.uri.gsub(/^.*var/,'/var')}`
                                `rm -rf #{event.recording.uri.gsub(/^.*var/,'/var').gsub(/\.wav$/,'_mono.wav')}`
                                end
	
=end
	                                $redis.publish "notification" , JSON.pretty_generate({from:$from_username, to:dest_username ,event:{name:"dial"} } )
					dial_status = dial dest , from: $from_number , confirm:ConfirmationController , confirm_metadata:{sip_call_id:sip_call_id  , from:$from_username, to:dest_username}
					 if dial_status.result != :answer
                                        $redis.publish "notification" , JSON.pretty_generate({from:$from_username, to:dest_username , event:{name:"dial_result", data:dial_status.result.to_s}})
 	                               end
	
                                hangup
                        end

                else
	                ringback = play! "/var/punchblock/system/ringback.wav"
=begin
			begin	
			call.on_joined {ringback.stop!}
			rescue Exception=>e
			logger.info e
			end 
=end			
                        logger.info "call from #{sip_agent} agent"
                        all_agents = JSON.parse(browser.get(GET_ALL_AGENTS).body)['agents']||[]
                        all_agents.each do |agent|
                                if agent['email'] == $from_number.gsub(/_at_/i, '@')
                                        if agent['caller_id']
						$from_number = agent['caller_id'].gsub(/[ ]*/,'')
					else
						$from_number = $from_number.gsub(/_at_/i, '@')
					end
                                end
                        end
                        logger.info "updated caller id is #{$from_number}"
                        answer
=begin
                         record async: true do |event|
                                      logger.info "Async recording for call id #{sip_call_id}  saved to #{event.recording.uri}"
                                        aws.push_recording("#{sip_call_id}.wav" , event.recording.uri.gsub(/^.*var/,'/var'))
                                        logger.info "posted data to update_call" + browser.post(POST_CALL_RECORDING,{"call_id"=> sip_call_id ,"file_path"=>LETMECALLU_RECORDING_BUCKET+sip_call_id+".wav" }).content
                                end
=end
                        to = call.to.gsub(/\:.*/,'').gsub(/\@.*/,'')
                        if( !to.match(/^00.*$/).nil? )
				 $redis.publish "notification" , JSON.pretty_generate({from:$from_username, to:$to_username ,event:{name:"dial"} } )
                                dial_status = dial "sofia/external/#{OUTGOING_PROVIDER_PREFIX}#{to.strip.gsub(/^00/,'')}@#{OUTGOING_PROVIDER}" , from: $from_number, confirm:ConfirmationController , confirm_metadata:{from:$from_username, to:$to_username , sip_call_id:sip_call_id   }

                                if dial_status.result != :answer
                                logger.info "====== notification ======\n dial_result \n =============================\n"       
				 $redis.publish "notification" , JSON.pretty_generate({from:$from_username, to:$to_username , event:{name:"dial_result", data:dial_status.result.to_s}})
                                end

                                hangup
                        elsif ( !to.match(/^\+.*$/).nil? )
				 $redis.publish "notification" , JSON.pretty_generate({from:$from_username, to:$to_username ,event:{name:"dial"} } )

                                dial_status = dial "sofia/external/#{OUTGOING_PROVIDER_PREFIX}#{to.strip.gsub(/^\+/,'')}@#{OUTGOING_PROVIDER}" , from: $from_number , confirm:ConfirmationController , confirm_metadata:{ sip_call_id:sip_call_id  ,   from:$from_username, to:$to_username}

                                if dial_status.result != :answer
                                        $redis.publish "notification" , JSON.pretty_generate({from:$from_username, to:$to_username , event:{name:"dial_result", data:dial_status.result.to_s}})
                                end

                                hangup
                        else
				logger.info "#{FS_CLI} -x \"sofia_contact #{call.to.gsub(/\:.*/,'')}\""
                                sip_uri = `#{FS_CLI} -x \"sofia_contact #{call.to.gsub(/\:.*/,'')}\"`.strip
                                logger.info "attempting dial to #{sip_uri}"
				 $redis.publish "notification" , JSON.pretty_generate({from:$from_username, to:$to_username ,event:{name:"dial"} } )

				dial_status =   dial "{hangup_after_bridge=false}#{sip_uri}" , from: $from_number , confirm:ConfirmationController , confirm_metadata:{ sip_call_id:sip_call_id  , from:$from_username, to:$to_username}

                                if dial_status.result != :answer
                                        $redis.publish "notification" , JSON.pretty_generate({from:$from_username, to:$to_username , event:{name:"dial_result", data:dial_status.result.to_s}})
                                end

				logger.info "dial_status is #{dial_status.inspect }"

			        hangup
                        end
                end
        end

after_call do 
	logger.info "after call reached ================================================"
	$redis.publish "notification" , JSON.pretty_generate({from:$from_username, to:$to_username, event:{name:"hangup"}})
end
end
