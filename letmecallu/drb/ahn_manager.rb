require 'json'
class AhnManager
	
	def fs_cli(cmd) 
		`#{FS_CLI} -x "#{cmd}"`.strip
	end

	def conference(origin,call_id, cparty )
		c_dial_string = fs_cli( "sofia_contact #{cparty}" )
		other_call_id = fs_cli("uuid_getvar #{call_id} bridge_uuid ")
		logger.info "A -> #{call_id}  B-> #{other_call_id} C -> #{cparty} "
		Adhearsion.active_calls[call_id]=nil
		Adhearsion.active_calls[other_call_id]=nil
		 fs_cli("uuid_transfer #{call_id} -both 0000#{cparty} XML public")
		logger.info "------------------------->"+ fs_cli( "conference 0000#{cparty} dial #{c_dial_string}  #{origin}")
	end	

        def transfer(origin,call_id, cparty )
		if( !cparty.match(/^00.*$/).nil? )
			c_dial_string = "sofia/external/#{OUTGOING_PROVIDER_PREFIX}#{cparty.strip.gsub(/^00/,'')}@#{OUTGOING_PROVIDER}"
		elsif( !cparty.match(/^\+.*$/).nil? )
			c_dial_string = "sofia/external/#{OUTGOING_PROVIDER_PREFIX}#{cparty.strip.gsub(/^\+/,'')}@#{OUTGOING_PROVIDER}"
		else
                	c_dial_string = fs_cli("sofia_contact #{cparty}")
		end
                other_call_id = fs_cli("uuid_getvar #{call_id} bridge_uuid ")
                logger.info "A -> #{call_id}  B-> #{other_call_id} C -> #{cparty} "
                Adhearsion.active_calls[call_id]=nil
                Adhearsion.active_calls[other_call_id]=nil
                 fs_cli("uuid_transfer #{call_id} -both 0000#{cparty} XML public")
                dial_result= fs_cli("conference 0000#{cparty} dial #{c_dial_string}  #{origin}")
	logger.info "^^^^^^^^^^^^^^^^^^^ #{dial_result}"
		sleep(3)
		logger.info "uuid_kill  ----------------------------------------- #{call_id}"
		fs_cli("uuid_kill #{call_id}")
	end
	def showActiveCall()
	logger.ifo"+++++++++++++++++++++++  #{Adhearsion.active_calls}"
	end

end
